﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using Awesomium.Core;
using MusicPlayer;
using NLog;

namespace Player.Pages
{
    /// <summary>
    /// Interaction logic for Authorization.xaml
    /// </summary>
    public partial class Authorization : Page
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();

        private string _token;

        public Authorization()
        {
            InitializeComponent();

            var values = string.Empty;
            var session = WebCore.CreateWebSession("D:\\myapp", WebPreferences.Default);

            session.SetCookie(new Uri(Properties.Resources.AuthorizationUri), values, false, false);

            MainWebControl.WebSession = session;
            MainWebControl.Source =
                new Uri(Properties.Resources.AuthorizationUri);
        }

        private void GetAddress_OnClick(object sender, RoutedEventArgs e)
        {
            var tmp = MainWebControl.Source.ToString();
            var token = GetToken(MainWebControl.Source.ToString());

            //var tmp1 = AddressTextBox.Text.IndexOf("&exp", StringComparison.Ordinal);
            //var token = tmp.Remove(AddressTextBox.Text.IndexOf("&exp", StringComparison.Ordinal));
            token = token.Remove(0, token.IndexOf("token=", StringComparison.Ordinal));
            token = token.Remove(0, token.IndexOf("=", StringComparison.Ordinal));
            token = token.Remove(0, 1);
            _token = token;

            var req = WebRequest.Create("https://api.vk.com/method/audio.get.json?user_id=22171101&access_token=" + token);
            var resp = req.GetResponse();

            var lst = new List<object>();
            var stream = resp.GetResponseStream();

            using (Stream file = File.Create("tmp.txt"))
            {
                CopyStream(stream, file);
            }
            JSonParser.GetAudios(File.ReadAllText("tmp.txt"));
        }

        private string GetToken(string uri)
        {
            var pattern = @"access_token=(.*?)\&";
            var match = Regex.Match(uri, pattern);
            if (match.Success)
                return match.Groups[1].Value;
            return string.Empty;
        }

        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }
        }
    }
}
