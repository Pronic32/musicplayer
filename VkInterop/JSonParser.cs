﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MusicPlayer.Sections;
using Newtonsoft.Json;

namespace MusicPlayer
{
    public static class JSonParser
    {
        public static List<Audio> GetAudios(string audios)
        {
            var type = new {response = new List<Audio>()};
            var tmp = JsonConvert.DeserializeAnonymousType(audios, type);
            return new List<Audio>();
        } 
    }
}
