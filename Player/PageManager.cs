﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Player
{
    public static class PageManager
    {
        /// <summary>
        /// Page stack
        /// </summary>
        private static readonly Stack<Page> PageStack;

        static PageManager()
        {
            PageStack = new Stack<Page>();
            CurrentWindow = Application.Current.MainWindow;
        }

        /// <summary>
        /// Push new page without current page closing 
        /// </summary>
        /// <param name="page"></param>
        public static void PushPageWithoutClose(Page page)
        {
            PageStack.Push(page);
            PushPage(page);
        }

        /// <summary>
        /// Push new page
        /// </summary>
        /// <param name="page"></param>
        public static void PushNewPage(Page page)
        {
            PushPage(page);
        }

        /// <summary>
        /// Return page from stack
        /// </summary>
        public static void ReturnPage()
        {
            if (PageStack.Any())
                PushPage(PageStack.Pop());
        }

        /// <summary>
        /// Push page
        /// </summary>
        /// <param name="page"></param>
        private static void PushPage(Page page)
        {
            if (CurrentWindow == null) return;
            CurrentWindow.Content = page;
            CurrentPage = page;
        }

        /// <summary>
        /// Main window 
        /// </summary>
        public static Window CurrentWindow { get; set; }

        /// <summary>
        /// Current page
        /// </summary>
        public static Page CurrentPage { get; private set; }
    }
}
