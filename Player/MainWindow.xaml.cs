﻿using System;
using System.Windows;
using NAudio;
using NAudio.Wave;
using Player.Pages;

namespace Player
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            //IWavePlayer waveOutDevice = new WaveOut();
            //var fileReader = new AudioFileReader("G:\\Наутилус Помпилиус - Лучшие песни. Новая коллекция (часть 1 и 2) (2007)\\Наутилус Помпилиус - Лучшие песни. часть 1 (2007)\\02. Матерь богов.mp3");
            //waveOutDevice.Init(fileReader);
            //waveOutDevice.Play();
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            PageManager.CurrentWindow = this;
            //var page = new Settings();
            //PageManager.PushNewPage(page);
            var page = new Authorization();
            PageManager.PushNewPage(page);
        }
    }
}
